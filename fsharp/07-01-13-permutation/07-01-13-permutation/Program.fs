﻿[<EntryPoint>]
let main argv = 
    let rec permute (toPermute:string) =
        let mutable permutations = Set.empty
        if toPermute.Length <= 1 then permutations <- permutations.Add toPermute
        else
            for i = 0 to toPermute.Length - 1 do
                for s in permute(toPermute.Remove(i,1)) do
                    permutations <- permutations.Add (toPermute.Substring(i,1) + s)
        permutations
    // run permute
    for str in permute("abbccc") do
        printfn "%s" str
    0