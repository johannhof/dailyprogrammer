<html>
<head></head>
<body>
    <?php

$json = file_get_contents("http://www.reddit.com/r/dailyprogrammer.json?limit=100", 0, null, null);
$json_output = json_decode($json, true);
$rawdata = $json_output['data']['children'];

while ($json_output['data']['after'] != null) {
    $json = file_get_contents("http://www.reddit.com/r/dailyprogrammer.json?limit=100&after=" . $json_output['data']['after'], 0, null, null);
    $json_output = json_decode($json, true);
    $rawdata = array_merge($rawdata, $json_output['data']['children']);
}

class Article {
    public $difficulty, $date, $title, $id, $url;
}
$articles = [];

foreach ($rawdata as $article) {
    if (preg_match_all("/[0-9]+\/[0-9]+\/[0-9]+/", $article['data']['title'], $matches) != 0) {
        $a = new Article();
        $a->date = $matches[0][0];
        preg_match_all("/\[\w*\].*/", $article['data']['title'], $matches);
        $a->title = preg_replace("/\[\w*\]/","",$matches[0][0]);
        preg_match_all("/\[\w*\]/", $article['data']['title'], $matches);
        $a->difficulty = substr($matches[0][0], 1, strlen($matches[0][0]) - 2);
        preg_match_all("/#\d*/", $article['data']['title'], $matches);
        $a->id = intval(substr($matches[0][0], 1, strlen($matches[0][0]) - 1));
        $a->url = $article['data']['url'];
        array_push($articles, $a);
    }
}

function sortId($a, $b) {
    if ($a->id > $b->id) {
        return 1;
    } elseif ($a->id == $b->id) {
        return 0;
    }
    return -1;
}

function sortDifficulty($a, $b) {
    $comp = strcmp(strtolower($a->difficulty), strtolower($b->difficulty));
    if ($comp != 0) {
        return $comp;
    }
    return sortId($a, $b);
}

if ($_GET['sort'] == "id") {
    usort($articles, "sortId");
} elseif ($_GET['sort'] == "difficulty") {
    usort($articles, "sortDifficulty");
}

echo "<table><th>ID</th><th>Date</th><th>Title</th><th>Difficulty</th><th>URL</th>";
for ($i = 0; $i < count($articles); $i++) {
    echo "<tr><td>#" . $articles[$i]->id . "</td>";
    echo "<td>" . $articles[$i]->date . "</td>";
    echo "<td>" . $articles[$i]->title . "</td>";
    echo "<td>" . $articles[$i]->difficulty . "</td>";
    echo "<td><a href='" . $articles[$i]->url . "'>Link</a></td></tr>";
}
echo "</table>";
echo "</body>";
echo "</html>";