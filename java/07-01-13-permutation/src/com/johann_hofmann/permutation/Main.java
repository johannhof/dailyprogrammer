package com.johann_hofmann.permutation;

import java.util.HashSet;

public class Main {

	public static void main(String[] args) {
		System.out.println(permute("foo"));
	}

	public static HashSet<String> permute(String toPermute) {
		HashSet<String> set = new HashSet<String>();
		if (toPermute.length() <= 1) {
			set.add(toPermute);
		} else {
			for (int i = 0; i < toPermute.length(); i++) {
				for (String s : permute(toPermute.substring(0, i) + toPermute.substring(i + 1))) {
					set.add(toPermute.substring(i, i + 1) + s);
				}
			}
		}
		return set;
	}
}
